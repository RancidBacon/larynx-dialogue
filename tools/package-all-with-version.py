#!/usr/bin/env python3

#
# Usage: GODOT_BINARY=/<path>/Godot_v3.4-stable_x11.64 ./tools/package-all-with-version.py [--dry-run] [--export-debug-(linux|win64)] [--export-release-(linux|win64)] src/export_presets.cfg
#

# Requires: `export_presets.cfg` to have at least one preset with valid export path.

# Produces: # TODO: List here...

# TODO: Add license etc.

# Note: Yes, this script will inevitably grow to the
#       point where it can send/receive email.

# TODO: Reimplement in GDScript? (I mean I *guess* I could've started with bash.)

# Other package/exporter projects:
#
#  * <https://github.com/firebelley/godot-export> (CI Action)
#
#  * <https://chibifire.com/posts/2019-07-20-gravity/>
#
#  * <https://bitbrain.github.io/2020/08/01/publish-godot-game-to-itchio.html>
#

import os
import sys
import glob
import subprocess
import configparser


class GodotExecutable:

    file_path = ""


    def __init__(self, executable_file_path):

        self.file_path = executable_file_path


    def show_version_test(self):

        return subprocess.run([self.file_path, "--version"])


    def _get_arguments_for_export(self, export_config):

        # TODO: Support headless & no-window.
        # TODO: Support release template.

        # NOTE: This does *not* work with headless because it treats 160x160 as a file path instead of erroring out.
        # return [self.file_path, "--windowed", "--resolution", "160x160", "--export%s" % export_config._template, export_config.preset_name , export_config.output_file_path]
#
        return [self.file_path, "--no-window", "--export%s" % export_config._template, export_config.preset_name , export_config.output_file_path, "--path", export_config.project_base_dir]



    def export_dry_run(self, export_config):

        # TODO: Handle mkdir so it's not created on dry_run.

        print(self._get_arguments_for_export(export_config))


    def export(self, export_config):

        subprocess.run(self._get_arguments_for_export(export_config))



class ExportConfig:

    def __init__(self):
        pass



def export_with_preset(export_config, godot_instance):

    # TODO: Make this a method of GodotExecutable?

    export_output_directory_no_bump = os.path.join(export_config.output_base_dir, export_config.platform.lower(), export_config.template,  "v%s" % (export_config.version))

    if os.path.exists(export_output_directory_no_bump):

        existing_bump_dirs = glob.glob("%s?" % export_output_directory_no_bump)

        if not existing_bump_dirs:
           export_config.revision_bump = "a" # TODO: Don't just overwrite if supplied?
        else:
           latest_revision_bump = sorted(existing_bump_dirs)[-1][-1]

           # TODO: Validate better...
           export_config.revision_bump = chr(ord(latest_revision_bump) + 1)

        if export_config.revision_bump == "z":
            raise NotImplementedError # TODO: Handle this situation.

    export_output_directory = "%s%s" % (export_output_directory_no_bump, export_config.revision_bump)

    os.makedirs(export_output_directory) # TODO: Don't error out if the directory exists but is empty to avoid wasting directories on failed exports?

    # TODO: Handle less hackily?
    if export_config.template == "release":
        export_config._template = ""
    else:
        export_config._template = "-debug"

    export_file_name = "{base_name}-{platform}{_template}-v{version}{revision_bump}.{extension}".format_map(export_config.__dict__) # TODO: Handle map properly.


    export_output_subdirectory = os.path.splitext(export_file_name)[0]


    msg_create_archive_hint = ""

    if export_config.extension in ["x86_64"]:

        # TODO: Change this so relative directories are correct.
        msg_create_archive_hint = "# tar czvf '%s/%s.tar.gz' --directory='%s' '%s'" % (export_output_directory, export_output_subdirectory, export_output_directory, export_output_subdirectory)

    elif export_config.extension in ["exe", "html"]:

        # TODO: Change this so relative directories are correct.
        msg_create_archive_hint = "# pushd '%s' && zip --recurse-paths '%s.zip' '%s' && popd" % (export_output_directory, export_output_subdirectory, export_output_subdirectory)


    export_output_directory = os.path.join(export_output_directory, export_output_subdirectory)
    os.makedirs(export_output_directory) # TODO: Don't error out if the directory exists but is empty to avoid wasting directories on failed exports?


    # Note: Hack to handle itch.io requirement that the HTML file in `.zip` is named `index.html`
    # TODO: Change handling of this so the other exported files don't get named "index.*"?
    if export_config.extension in ["html"]:
        export_file_name = "index.html"


    export_config.output_file_path = os.path.join(export_output_directory, export_file_name)


    print("")

    if export_config.dry_run:
        godot_instance.export_dry_run(export_config)
    else:
        godot_instance.export(export_config)

    # TODO: Return (and collect) output file paths.

    print()
    print(">> output: %s <<" % export_output_directory)

    print()
    print(msg_create_archive_hint)
    print()

    print("")



if __name__=="__main__":

   godot_binary_file_path = os.getenv("GODOT_BINARY")
   if not godot_binary_file_path:
      print("Please set GODOT_BINARY environment variable to Godot binary file path.")
      raise SystemExit

   godot = GodotExecutable(godot_binary_file_path)

   print("")
   godot.show_version_test()
   print("")


   file_path_config = "export_presets.cfg"

   if (len(sys.argv) > 1) and not (sys.argv[1:][-1].startswith("--")):

       file_path_config = sys.argv[1:][-1]


   if not os.path.exists(file_path_config):

        print("Path not found: '%s'" % file_path_config)

        raise SystemExit

   if not os.path.isfile(file_path_config):

        print("Path not a file: '%s'" % file_path_config)

        raise SystemExit

   print("Using presets file: '%s'" % file_path_config)


   # TODO: Exit if no export presets config.
   export_presets = configparser.ConfigParser()
   export_presets.read(file_path_config)
   # TODO: Add this?  export_presets.read(os.path.join("src", "export_presets.cfg"))

   # TODO: Enable non --all option

   for section_name in export_presets.sections():

       section = export_presets[section_name]

       if "name" in section.keys():

          print()
          print("Preset name: %s" % section["name"])
          print()

          export_preset_name = section["name"].strip('"')

          path_parts = section["export_path"].strip('"').rsplit("/", 3)

          export_output_base_dir = path_parts[0]


          project_base_dir = os.path.split(os.path.abspath(file_path_config))[0]

          if not os.path.isabs(export_output_base_dir):

              export_output_base_dir = os.path.normpath(os.path.join(project_base_dir, export_output_base_dir))

          print("Export base directory: '%s'" % export_output_base_dir)


          export_platform = path_parts[1].capitalize()
          export_version = path_parts[2].lstrip("v") # TODO: Handle revision bump? Retrieve from elsewhere?

          filename_parts = path_parts[3].rsplit("-", 1)

          export_base_name = filename_parts[0]
          export_extension = filename_parts[-1].rsplit(".")[-1]


          export_config_for_preset = ExportConfig()

          # TODO: Set these properly?
          export_config_for_preset.preset_name = export_preset_name

          export_config_for_preset.project_base_dir = project_base_dir

          export_config_for_preset.output_base_dir = export_output_base_dir
          export_config_for_preset.platform = export_platform
          export_config_for_preset.version = export_version

          export_config_for_preset.base_name = export_base_name
          export_config_for_preset.extension = export_extension

          export_config_for_preset.project_name = export_base_name # TODO: Handle properly.
          export_config_for_preset.revision_bump = "" # TODO: Handle properly?

          export_config_for_preset.dry_run = ("--dry-run" in sys.argv)

          export_all = not any(filter(lambda option_arg : option_arg.startswith("--export-"), sys.argv[1:]))

          if export_config_for_preset.dry_run:
              print(export_config_for_preset.__dict__)

          if export_all or ("--export-debug-%s" % export_config_for_preset.platform.lower() in sys.argv):
            export_config_for_preset.template = "debug" # TODO: Handle properly.
            export_with_preset(export_config_for_preset, godot)

          if export_all or ("--export-release-%s" % export_config_for_preset.platform.lower() in sys.argv):
            export_config_for_preset.template = "release" # TODO: Handle properly.
            export_with_preset(export_config_for_preset, godot)

          # TODO: Add automatic archive creation where required.

          # TODO: Only export one Mac option and/or handle directory naming better.
          # TODO: Don't attempt DMG export on non-Mac.

          print()


   # TODO: Handle export_template.
   # TODO: Update presets file with new (base) version value in path?
   # TODO: Update all references to version in the presets file?

   #export_project_name = "ForeignerExportDemo"
   ##export_base_name = "ForeignerExportDemo"
   ##export_version="0.1.1"
   ##export_revision_bump = "f"
   #export_revision_bump = ""
   #export_template = "release" # "debug"
   ##export_extension = "exe" # or ...
   ##export_platform = "Win64" # or "Mac" or "Linux"

   # "exports/<platform>/v<version>[<bump>]/<ForeignerExportDemo>-<short_platform>-[<template>]-v<version>[<bump>].<ext>"
