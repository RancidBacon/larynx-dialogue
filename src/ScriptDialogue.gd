extends MarginContainer

onready var UI_LINES: VBoxContainer = self.find_node("Lines", true, false)

func _ready() -> void:

    pass


var speaker_names = {}

var speaker_voices = {}


func add_dialogue_line(dialog_line: String):

    var the_line = preload("res://DialogueLine.tscn").instance()

    UI_LINES.add_child(the_line, true)


    # TODO: Move more of the parsing and/UI control into `DialogueLine` scene?

    var speaker_text: String = ""
    var words_text: String = ""

    var is_comment: bool = dialog_line.begins_with("#")

    the_line.find_node("btnIsActive", true, false).pressed = not is_comment
    the_line.find_node("btnIsActive", true, false).visible = not is_comment

    if is_comment:

        speaker_text = ""
        words_text = dialog_line

        the_line.find_node("lblWords", true, false).self_modulate = Color(1, 1, 1, 0.5)
        the_line.find_node("lblSpeaker", true, false).self_modulate = Color(1, 1, 1, 0.5)

        if dialog_line.begins_with("## "):
            var config_line: String = dialog_line.lstrip("# ")

            match Array(config_line.split(":", true, 1)):

                [var the_speaker_name_, var the_voice_id_]:

                    var the_speaker_name: String = the_speaker_name_.strip_edges()
                    var the_voice_id: String = the_voice_id_.strip_edges()

                    if the_voice_id:

                        self.speaker_voices[the_speaker_name] = the_voice_id
                        prints(the_speaker_name, the_voice_id)

                _:
                    push_warning("Unknown config line: '%s'." % dialog_line)

    else:

        match Array(dialog_line.split(":", true, 1)):

            [var speaker_name, var words_text_]:

                speaker_text = "%s:" % speaker_name.strip_edges()
                words_text = words_text_.strip_edges()

                self.speaker_names[speaker_name] = "" # TODO: Handle different?

            _:

                # TODO: Handle indent/dittos/malformed lines?

                words_text = dialog_line

                the_line.find_node("btnIsActive", true, false).visible = false

    the_line.find_node("lblWords", true, false).text = words_text
    the_line.find_node("lblSpeaker", true, false).text = speaker_text

    if speaker_text == "":

        the_line.find_node("btnIsActive", true, false).pressed = false

        the_line.find_node("lblWords", true, false).self_modulate = Color(1, 1, 1, 0.5)


func get_lines():

    return UI_LINES.get_children()
