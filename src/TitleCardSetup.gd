#
# Generate "Title Cards" to be used with recorded output...
#
#
# # ffmpeg -f concat -i <path>/concat-config.txt -i <path>/tts-audition-all-abc.wav -pix_fmt yuv420p <path>/output.mp4
#
#
# For ffmpeg CLI, consulted these references:
#
#  * <https://trac.ffmpeg.org/wiki/Slideshow#Concatdemuxer>
#
#  * <https://trac.ffmpeg.org/wiki/Slideshow#Singleimage>
#
#  * <https://trac.ffmpeg.org/wiki/Slideshow#Addingaudio>
#
#    Note: The audio example doesn't include the `-pix_fmt yuv420p` option which
#          FF and Twitter both seem to want.
#
#
# For Twitter compatible output, consulted these references:
#
#  * <https://gist.github.com/nikhan/26ddd9c4e99bbf209dd7#gistcomment-2593046> (by foone)
#
#  * <https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/uploading-media/media-best-practices>
#
#
# For local browser (FF) playback testing, consulted these references:
#
#  * <https://support.mozilla.org/en-US/kb/html5-audio-and-video-firefox#w_supported-formats>
#
#  * <http://camendesign.com/code/video_for_everybody/test.html>
#
#
# This tool may also have been useful:
#
#  * <http://manpages.ubuntu.com/manpages/precise/man1/mediainfo.1.html>
#

extends Node

class_name TitleCardSetup


var partial_filename = ""

func _ready() -> void:

    $PanelContainer/MarginContainer/VBoxContainer/Label.text = "Larynx TTS"
    $PanelContainer/MarginContainer/VBoxContainer/Label2.text = "Voice Sample"

    partial_filename = text_to_partial_file_name($PanelContainer/MarginContainer/VBoxContainer/Label.text) + "--" + \
        text_to_partial_file_name($PanelContainer/MarginContainer/VBoxContainer/Label2.text)


func _on_btnCapture_pressed() -> void:

    $PanelContainer/MarginContainer/VBoxContainer/btnCapture.visible = false

    yield(VisualServer, "frame_post_draw")

    var img = get_viewport().get_texture().get_data()

    img.flip_y()

    var output_file_path: String = "/tmp/output--%s.png" % self.partial_filename.to_lower()

    img.save_png(output_file_path)

    print(output_file_path)

    $PanelContainer/MarginContainer/VBoxContainer/btnCapture.visible = true


static func text_to_partial_file_name(text: String) -> String:

    # TODO: Make more robust...

    return text.replace(" ", "-").replace("/","__")
