extends MarginContainer

var speaker_name: String setget _set_speaker_name
var speaker_voice: String setget _set_speaker_voice

var override_voices: Array setget _set_override_voices

var active_override_voice: String setget ,_get_active_override_voice


func _get_active_override_voice() -> String:

    return $VBoxContainer/HBoxContainer/selectedVoiceOverride.text


func _set_speaker_name(new_value: String):

    speaker_name = new_value

    $VBoxContainer/HBoxContainer/lblSpeakerName.text = self.speaker_name


func _set_speaker_voice(new_value: String):

    speaker_voice = new_value

    $VBoxContainer/HBoxContainer/lblSpeakerVoice.text = self.speaker_voice


func _set_override_voices(new_override_voices: Array):

    override_voices = new_override_voices

    $VBoxContainer/HBoxContainer/selectedVoiceOverride.clear()

    $VBoxContainer/HBoxContainer/selectedVoiceOverride.add_item("----")

    for current_voice_id in self.override_voices:

        $VBoxContainer/HBoxContainer/selectedVoiceOverride.add_item(current_voice_id)

    # TODO: Handle when previously selected voice no longer available?
    #       (Probably not necessary.)

    # TODO: Reselect previously selected voice?


func _ready() -> void:
    pass
