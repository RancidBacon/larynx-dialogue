extends MarginContainer


func is_active() -> bool:

    return self.find_node("btnIsActive", true, false).pressed


# TODO: Handle this less hackily.
func get_speaker_name() -> String:
    return self.find_node("lblSpeaker", true, false).text.rstrip(":")


# TODO: Handle this less hackily.
func get_words() -> String:
    return self.find_node("lblWords", true, false).text


func _ready() -> void:
    pass
