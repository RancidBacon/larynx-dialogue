extends Node

onready var UI_EXPORT_CONFIG_CONTAINER: Container = self.find_node("ExportConfigContainer", true, false)
onready var UI_EXPORT_FILE_PATH_LINE_EDIT: LineEdit = UI_EXPORT_CONFIG_CONTAINER.find_node("ExportFilePathLineEdit", true, false)

onready var UI_SELECTED_VOICE: OptionButton = self.find_node("SelectedVoice", true, false)
onready var UI_DIALOGUE_LINE_EDIT: LineEdit = self.find_node("DialogueLineEdit", true, false)

onready var UI_SCRIPT_FILE_PATH: LineEdit = self.find_node("ScriptFilePath", true, false)
onready var UI_FILE_DIALOG: FileDialog = self.find_node("FileDialog", true, false)

onready var UI_SPEAKER_CONFIG_CONTAINER: Container = self.find_node("SpeakerConfigContainer", true, false)

onready var UI_SCRIPT_CONTAINER: Container = self.find_node("ScriptContainer", true, false)

onready var UI_BUTTON_RECORD_OUTPUT: Button = self.find_node("btnRecordOutput", true, false)


signal start_audio_recording(output_file_path)
signal stop_audio_recording



func speak(text: String, voice: String):

    $Larynx.text_to_speech(text, voice)


func speak_selected():

    self.speak(UI_DIALOGUE_LINE_EDIT.text, UI_SELECTED_VOICE.text)


func output_recording_is_enabled() -> bool:

    return UI_BUTTON_RECORD_OUTPUT.pressed


func get_output_directory() -> String:

    var output_directory: String = UI_EXPORT_FILE_PATH_LINE_EDIT.text

    if not output_directory.is_abs_path():

        output_directory = $Larynx.get_project_base_path().plus_file(output_directory) # TODO: Move `get_project_base_path()` method into utils.

    return output_directory


func generate_output_filepath(name_modifier: String = ""):

    var output_directory: String = self.get_output_directory()

    # TODO: Don't create here?
    if not Directory.new().dir_exists(output_directory):

        prints("Need to create:", output_directory)

        var err = Directory.new().make_dir_recursive(output_directory)
        if err != OK:
            push_error("Could not create output directory: %s" % err)
            return
        else:
            prints("Created output directory:", output_directory)


    var filename_without_extension: String = "tts"

    if name_modifier:
        name_modifier = "-%s" % name_modifier

    filename_without_extension =  "%s%s-%x" % [filename_without_extension, name_modifier, OS.get_system_time_msecs()]

    var filename: String = "%s.wav" % filename_without_extension

    return output_directory.plus_file(filename)



func _ready() -> void:

    UI_EXPORT_CONFIG_CONTAINER.visible = false

    UI_FILE_DIALOG.current_dir =  $Larynx.get_project_base_path() # TODO: Move method into utils.

    $Larynx.retrieve_voice_list()




func _on_Button_pressed() -> void:

    emit_signal("start_audio_recording", self.generate_output_filepath("audition-solo"))

    self.speak_selected()

    yield($AudioStreamPlayer, "finished")

    emit_signal("stop_audio_recording")



signal speaker_active_event(speaker_id)

func _on_btnAuditionAll_pressed() -> void:

    self.audition_multiple__with_stupid_yield(true)

    #
    # NOTE: DO NOT PUT ANY CODE HERE, AS IT WILL RUN IMMEDIATELY WITHOUT
    #       WAITING FOR "yield()" CALL TO COMPLETE!!!
    #
    #       (I WILL CERTAINLY LIVE TO REGRET DOING THIS.)
    #
    #print("foo")


func _on_btnAuditionSome_pressed() -> void:

    self.audition_multiple__with_stupid_yield(false)

    #
    # NOTE: DO NOT PUT ANY CODE HERE, AS IT WILL RUN IMMEDIATELY WITHOUT
    #       WAITING FOR "yield()" CALL TO COMPLETE!!!
    #
    #       (I WILL CERTAINLY LIVE TO REGRET DOING THIS.)
    #
    #print("foo")


const AUDITION_SOME__MAX_VOICE_COUNT_LIMIT: int = 3

func audition_multiple__with_stupid_yield(audition_all: bool = true):

    var limit: int = UI_SELECTED_VOICE.get_item_count()
    var file_name_segment: String = "all"

    if not audition_all:
        limit = int(min(AUDITION_SOME__MAX_VOICE_COUNT_LIMIT, limit))
        file_name_segment = "some"


    emit_signal("start_audio_recording", self.generate_output_filepath("audition-%s" % file_name_segment))

    for selected_index in range(limit):

        UI_SELECTED_VOICE.selected = selected_index

        emit_signal("speaker_active_event", UI_SELECTED_VOICE.text) # TODO: Make duration calcs etc easier by having a speaker "finish" event too?

        self.speak_selected()

        yield($AudioStreamPlayer, "finished")

    emit_signal("stop_audio_recording")



func _on_btnOpenScriptFile_pressed() -> void:

    UI_FILE_DIALOG.popup_centered_ratio()


func _on_FileDialog_file_selected(path: String) -> void:

    UI_SCRIPT_FILE_PATH.text = path

    # TODO: Actually load file.
    self.load_script_dialogue(UI_SCRIPT_FILE_PATH.text)


signal speaker_list_updated(speaker_names)


func load_script_dialogue(file_path: String):

    # Remove previous if it exists...
    for the_child in UI_SCRIPT_CONTAINER.get_children():

        UI_SCRIPT_CONTAINER.remove_child(the_child)
        the_child.queue_free()


    # Load new...
    var script_dialogue = preload("res://ScriptDialogue.tscn").instance()

    UI_SCRIPT_CONTAINER.add_child(script_dialogue, true)


    var file: File = File.new()

    var err = file.open(file_path, File.READ)

    if err != OK:
        push_error("Got error '%s' with path '%s'." % [err, file_path])
        return


    while true:

        var new_line_of_dialogue: String = file.get_line()

        if not file.eof_reached():
            script_dialogue.add_dialogue_line(new_line_of_dialogue)
        else:
            break


    emit_signal("speaker_list_updated", script_dialogue.speaker_names)


func _on_btnPlayScript_pressed() -> void:

    var active_override_voices: Dictionary = {}

    # TODO: Handle better...
    for current_speaker_config in UI_SPEAKER_CONFIG_CONTAINER.get_child(0).get_children():

        prints(current_speaker_config.speaker_name,
                current_speaker_config.speaker_voice,
                current_speaker_config._get_active_override_voice())

        # TODO: Look this up on the fly per-line to be more responsive?
        if current_speaker_config._get_active_override_voice() != "----":

            active_override_voices[current_speaker_config.speaker_name] = current_speaker_config._get_active_override_voice()


    var script_dialogue = UI_SCRIPT_CONTAINER.get_child(0) # TODO: Handle properly.

    print(script_dialogue.speaker_names)


    emit_signal("start_audio_recording", self.generate_output_filepath("script-read"))

    for current_line in script_dialogue.get_lines():

#        prints(current_line.is_active(), current_line.get_speaker_name(), script_dialogue.speaker_names.keys().find(current_line.get_speaker_name()))

        if current_line.is_active():

            # Note: Assumes stable order of keys...
            # TODO: Handle better.
            var speaker_index: int = script_dialogue.speaker_names.keys().find(current_line.get_speaker_name())

#            print(UI_SELECTED_VOICE.get_item_text(speaker_index))

#            print(current_line.get_words())

            var speaker_voice_id: String = ""

            if script_dialogue.speaker_voices.has(current_line.get_speaker_name()):

                speaker_voice_id = script_dialogue.speaker_voices[current_line.get_speaker_name()]

            else:

                speaker_voice_id = UI_SELECTED_VOICE.get_item_text(speaker_index)


            # TODO: Don't run redundant code above when overridden here?
            if active_override_voices.has(current_line.get_speaker_name()):

                speaker_voice_id = active_override_voices[current_line.get_speaker_name()]

                print(speaker_voice_id)


            # TODO: Handle this different when it's a script read through?
            emit_signal("speaker_active_event", speaker_voice_id) # TODO: Make duration calcs etc easier by having a speaker "finish" event too?

            self.speak(current_line.get_words(), speaker_voice_id)

            yield($AudioStreamPlayer, "finished")


    emit_signal("stop_audio_recording")



func _on_Larynx_available_voices_updated(voice_ids) -> void:

    UI_SELECTED_VOICE.clear()

    for voice_name in voice_ids:
        UI_SELECTED_VOICE.add_item(voice_name)

    self.update_voice_ids(voice_ids)


func update_voice_ids(voice_ids: Array):

    # TODO: Handle better...
    for current_speaker_config in UI_SPEAKER_CONFIG_CONTAINER.get_child(0).get_children():

        current_speaker_config.override_voices = voice_ids


func _on_MainUI_speaker_list_updated(speaker_names) -> void:

    # Remove previous if they exists...
    for the_child in UI_SPEAKER_CONFIG_CONTAINER.get_child(0).get_children():

        UI_SPEAKER_CONFIG_CONTAINER.get_child(0).remove_child(the_child)
        the_child.queue_free()


    # Load new...
    var script_dialogue = UI_SCRIPT_CONTAINER.get_child(0) # TODO: Handle properly.

    for current_name in speaker_names:

        var speaker_config = preload("res://SpeakerConfig.tscn").instance()

        speaker_config.speaker_name = current_name

        UI_SPEAKER_CONFIG_CONTAINER.get_child(0).add_child(speaker_config, true)

        var speaker_voice_id: String = "----"

        if script_dialogue.speaker_voices.has(current_name):

            speaker_voice_id = script_dialogue.speaker_voices[current_name]

        speaker_config.speaker_voice = speaker_voice_id


    self.update_voice_ids($Larynx.available_voices) # TODO: Handle better/properly.



class RecordingMetadata:

    var start_time_ms: int = 0
    var stop_time_ms: int = 0

    var events: Array = []

    func start():

        self.start_time_ms = OS.get_ticks_msec()

        self.events.append({'speaker_id': "<start>", 'time_stamp': OS.get_ticks_msec()}) # Hack


    func stop():

        self.stop_time_ms = OS.get_ticks_msec()

        self.events.append({'speaker_id': "<stop>", 'time_stamp': OS.get_ticks_msec()}) # Hack


        ## Calculate the duration for each speaker. (Hackily)
        var previous_event: Dictionary

        for event in self.events:

            if event.speaker_id != "<start>":

                previous_event["duration_ms"] = event.time_stamp - previous_event.time_stamp

                previous_event.time_stamp -= self.start_time_ms # TODO: Handle differently?

            previous_event = event

        # Remove the "<start>"/"<stop>" hacky events.
        self.events.pop_back()
        self.events.pop_front()


    func add_speaker_active_event(speaker_id: String):

        self.events.append({'speaker_id': speaker_id, 'time_stamp': OS.get_ticks_msec()})


    func debug_dump():

        print()
        print(self.start_time_ms-self.start_time_ms)

        for event in self.events:

#            prints(event.speaker_id, event.time_stamp-self.start_time_ms, event.duration_ms)
            prints(event.speaker_id, event.time_stamp, event.duration_ms)

        print(self.stop_time_ms-self.start_time_ms)
        print()



    func as_concat_config(title_card_file_path_prefix_hack: String = "") -> String:

        var lines: PoolStringArray = Array()

        for event in self.events:

            var title_card_file_path: String = TitleCardSetup.text_to_partial_file_name(event.speaker_id)

            lines.append("file '%s%s.png'" % [title_card_file_path_prefix_hack, title_card_file_path])

            lines.append("duration %f" % (event.duration_ms/1000.0))

        return lines.join("\n")


var active_recording_metadata: RecordingMetadata


var audio_recorder: AudioEffectRecord = AudioServer.get_bus_effect(AudioServer.get_bus_index("Master"), 0)

func _on_MainUI_start_audio_recording(output_file_path) -> void:

    if not self.output_recording_is_enabled():
        # Note: We do this to reduce code duplication at signal location.
        return

    self.active_recording_metadata = RecordingMetadata.new()
    self.active_recording_metadata.start()

    self.audio_recorder.set_meta("output_file_path", output_file_path) # TODO: Handle better?
    self.audio_recorder.set_recording_active(true)


func _on_MainUI_stop_audio_recording() -> void:

    if not self.audio_recorder.is_recording_active():
        # Note: We also do this to reduce code duplication at signal location.
        return

    self.active_recording_metadata.stop()

    self.audio_recorder.set_recording_active(false)

    var audio_recording: AudioStreamSample = self.audio_recorder.get_recording()

    var output_file_path: String = self.audio_recorder.get_meta("output_file_path")

    if not output_file_path:
        # Note: Default directory will be within project.
        output_file_path = "recording-default-filename.wav"

    print(output_file_path)

    audio_recording.save_to_wav(output_file_path)


    self.active_recording_metadata.debug_dump()

    print("----")

    print(self.active_recording_metadata.as_concat_config("output--larynx-tts--"))

    print("----")



func _on_MainUI_speaker_active_event(speaker_id) -> void:

    if self.audio_recorder.is_recording_active():

        self.active_recording_metadata.add_speaker_active_event(speaker_id)

#   prints("Active speaker:", speaker_id)


func _on_btnRecordOutput_toggled(button_pressed: bool) -> void:

    UI_EXPORT_CONFIG_CONTAINER.visible = button_pressed
