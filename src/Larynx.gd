#
# Note: This is a bit of a hack currently and I don't *really* want to be
#       running a server to make this work but it reduces latency for now.
#
#       Hopefully this can provide some abstraction so we can change to
#       use e.g. CLI interface in future.
#

# NOTE: This requires the Larynx HTTP server to be running already!

extends Node

export var larynx_host: String = "127.0.0.1"
export var larynx_port: int = 5002

const CACHE_DIR_PATH: String = "tts_wav_cache"

signal available_voices_updated(voice_ids)

var available_voices: Array = []


static func get_project_base_path():

    # via `ProjectSettings` docs.

    var path = ""
    if OS.has_feature("editor"):
        # Running from an editor binary.
        # `path` will contain the absolute path to `hello.txt` located in the project root.
        path = ProjectSettings.globalize_path("res://")
        path = path.plus_file("..")
    else:
        # Running from an exported project.
        # `path` will contain the absolute path to `hello.txt` next to the executable.
        # This is *not* identical to using `ProjectSettings.globalize_path()` with a `res://` path,
        # but is close enough in spirit.
        path = OS.get_executable_path().get_base_dir()

    return path


func get_cache_id_from_request_info(request_info: Dictionary):

    var text: String = request_info["text"]
    var voice_id: String = request_info["voice"]

    # TODO: Handle better?
    var cache_id: String = "tts-wav-%08x-%08x" % [voice_id.hash(), text.hash()]

#    prints(cache_id)

    return cache_id


func get_semi_sanitized_filename_from_request_info(request_info: Dictionary):

    var cache_id: String = self.get_cache_id_from_request_info(request_info)
    var cache_id_tail = cache_id.trim_prefix("tts-wav") # Hacks...

    var text: String = request_info["text"]
    var voice_id: String = request_info["voice"].split("/")[1].split("-")[0]

    # TODO: Handle better?
    var semi_name: String = "tts-%s-%s" % [voice_id.left(10), text.left(20)]

    semi_name = semi_name.to_lower()
    semi_name = semi_name.validate_node_name()
    semi_name = semi_name.replace(" ", "")
    semi_name = semi_name.replace("!", "")
    semi_name = semi_name.replace("_", "")
    semi_name = semi_name.replace("?", "")
    semi_name = semi_name.replace(",", "")

    semi_name = "%s%s" % [semi_name, cache_id_tail]
    prints(semi_name)

    return semi_name



func get_cache_base_path():

    # TODO: Handle this better.
    #       (Don't want to use user directory, as I want to make files easier to find.)
    var cache_path: String = self.get_project_base_path().plus_file(CACHE_DIR_PATH)

    return cache_path


func cache_tts_wav(request_info: Dictionary, audio: AudioStreamSample, name_override = ""):


    var cache_id: String = self.get_cache_id_from_request_info(request_info)

    var cache_path: String = self.get_cache_base_path()

    prints("Cache path:", cache_path)

    if not Directory.new().dir_exists(cache_path):

        prints("Need to create:", cache_path)

        var err = Directory.new().make_dir(cache_path)
        if err != OK:
            push_error("Could not create cache directory: %s" % err)
            return
        else:
            prints("Created cache directory:", cache_path)

    if Directory.new().dir_exists(cache_path):

        var expected_cached_file_path: String = cache_path.plus_file("a-%s.wav" % cache_id)

        if name_override:
            expected_cached_file_path = cache_path.plus_file(name_override)

        if not Directory.new().file_exists(expected_cached_file_path):

            prints(expected_cached_file_path)

            var err = audio.save_to_wav(expected_cached_file_path)

            if err != OK:
                push_warning("Could not cache output. Got error '%s' with path '%s'." % [err, expected_cached_file_path])
                return

        else:

            push_warning("Cache file already exists: %s" % expected_cached_file_path)


func _ready() -> void:
    pass


func get_voices():

    # TODO: Retrieve voice list dynamically?

    return [
        "en-us/southern_english_female-glow_tts",
        "en-us/mary_ann-glow_tts",
        "en-us/cmu_ahw-glow_tts",
        ]


const ENABLE_MORE_LEGIBLE_CACHE: bool = true

func text_to_speech(text: String, voice: String = "en-us/southern_english_female-glow_tts") -> PoolByteArray:

    # TODO: Handle this better.

    var cache_path: String = self.get_cache_base_path()
    var cache_id: String = self.get_cache_id_from_request_info({"text": text, "voice": voice})
    var expected_cached_file_path: String = cache_path.plus_file("a-%s.wav" % cache_id)

    print("")

    var request_info: Dictionary = {"text": text, "voice": voice}

    print(self.get_semi_sanitized_filename_from_request_info(request_info) )

    if Directory.new().file_exists(expected_cached_file_path):
        prints("Found:", expected_cached_file_path)

        var speech_audio_stream: AudioStreamSample = GDScriptAudioImport.load_wav_from_file(expected_cached_file_path)

        if ENABLE_MORE_LEGIBLE_CACHE:
            var name_override: String = "%s.wav" % self.get_semi_sanitized_filename_from_request_info(request_info) # Hacks...
            if not Directory.new().file_exists(cache_path.plus_file(name_override)): # Hacks...
                prints("Writing:", name_override)
                self.cache_tts_wav(request_info, speech_audio_stream, name_override) # Hacks...

        # TODO: Meh...
        $"../AudioStreamPlayer".stream = speech_audio_stream

        $"../AudioStreamPlayer".play()

        return PoolByteArray([]) # TODO: Meh.


    # TODO: Handle/indicate failure state better?

    var api_path: String = "api/tts"
    var query_string: String = HTTPClient.new().query_string_from_dict({"text": text, "voice": voice, "inlinePronunciations": "true"})

    var url: String = "http://%s:%d/%s?%s" % [larynx_host, larynx_port, api_path, query_string]

    prints(url)

    var err = $HTTPRequest.request(url)
    if err != OK:
        push_error("An error occurred in the HTTP request.")

    # TODO: Handle better.
    # TODO: Store more config/request details.
    $HTTPRequest.set_meta("active_request_info", {"text": text, "voice": voice})

    # TODO: Meh, wait for response.

    return PoolByteArray([])


func _on_HTTPRequest_request_completed(result: int, response_code: int, _headers: PoolStringArray, body: PoolByteArray) -> void:

#    prints(result, response_code)

    if result == 0 and response_code == 200:

        var active_request_info: Dictionary = $HTTPRequest.get_meta("active_request_info")

        print(active_request_info)

#        prints(body)

        var speech_audio_stream = GDScriptAudioImport.load_wav_from_bytes(body)

#        prints(speech_audio_stream)

#        var speech_audio_stream: AudioStreamSample = AudioStreamSample.new()
#        speech_audio_stream.data = body

        # TODO: Meh...
        $"../AudioStreamPlayer".stream = speech_audio_stream

        $"../AudioStreamPlayer".play()


        self.cache_tts_wav(active_request_info, speech_audio_stream)


    $HTTPRequest.remove_meta("active_request_info")


func retrieve_voice_list():

    #----

    var api_path: String = "api/voices"
    var url: String = "http://%s:%d/%s" % [larynx_host, larynx_port, api_path]

    prints(url)

    var err = $HTTPRequest_VoiceList.request(url)
    if err != OK:
        push_error("An error occurred in the HTTP request.")

    #----


func _on_HTTPRequest_VoiceList_request_completed(result: int, response_code: int, _headers: PoolStringArray, body: PoolByteArray) -> void:

    var now_available_voices: Array = []

    if result == 0 and response_code == 200:

        var voice_list_json = body.get_string_from_utf8()

        var voice_list_json_parsed = parse_json(voice_list_json)


        for current_voice_info in voice_list_json_parsed.values():

            if current_voice_info["downloaded"] == true:
#                prints(current_voice_info["id"], current_voice_info["downloaded"])

                now_available_voices.append(current_voice_info["id"])

        if now_available_voices.size() > 0: # TODO: Still do this when no voices?

            self.available_voices = now_available_voices

            emit_signal("available_voices_updated", now_available_voices)

    else:

        prints(result, response_code)
