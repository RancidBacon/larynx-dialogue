### Multi-speaker Script Dialogue Tool for Larynx Text To Speech

Status: Proof-of-concept

Implemented with Godot.

#### Dependencies

 * https://github.com/rhasspy/larynx

#### Requirements

 * Some Larynx voices need to be installed.

 * The `larynx-server` HTTP server needs to be running before using
   the application.

#### Notes

 * Godot unexpectedly doesn't currently support parsing WAV files at
   run time so this code uses a [lightly modified version](https://gitlab.com/RancidBacon/larynx-dialogue/-/blob/main/src/GDScriptAudioImport.gd)
   of <https://github.com/Gianclgar/GDScriptAudioImport/> to do so.

   Related Godot proposals/PRs:

    * <https://github.com/godotengine/godot-proposals/issues/732>
    * <https://github.com/godotengine/godot/pull/47389>
